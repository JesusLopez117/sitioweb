// Import the functions you need from the SDKs you need
import { initializeApp } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import{ getDatabase,onValue,ref,set,child,get,update,remove}
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

import{getStorage, ref as refs, uploadBytes, getDownloadURL}
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";


// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCbQp88i8Amsvxm6k6ZDUt8zDlCe76EJMc",
  authDomain: "webfinal-99dae.firebaseapp.com",
  projectId: "webfinal-99dae",
  storageBucket: "webfinal-99dae.appspot.com",
  messagingSenderId: "1038257620051",
  appId: "1:1038257620051:web:76abd1e067f2678827f2a6"
};

//Inicializacion del Firebase.
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const str = getStorage();

//Declaración de los botones.
var btnGuardar = document.getElementById('btnGuardar');
var btnActualizar = document.getElementById('btnActualizar');
var btnConsultar = document.getElementById('btnConsultar');
var btnDesha = document.getElementById('btnDeshabilitar');
var btnLimpiar = document.getElementById('btnLimpiar');
var btnHabilitar = document.getElementById('btnHabilitar')
var archivo = document.getElementById('archivo');
var lis = document.getElementById('lista');

//Variables de los inputs globales
var codigo = 0;
var nombre = "";
var descripcion = "";
var precio = "";
var imagen = "";
var estatus = "0";
var link = "";

//Función para leer los Inputs.
const leerInputs = () =>{
    codigo = document.getElementById('codigo').value;
    nombre = document.getElementById('nombre').value;
    descripcion = document.getElementById('descripcion').value;
    precio = document.getElementById('precio').value;
    imagen = document.getElementById('imgNombre').value;
    estatus = "0";
    console.log(document.getElementById('url').value);
}

//Función para insertar datos  en la base de datos.
async function insestarDatos(){
  
  let arch = document.getElementById('imgNombre').value;
  const starsRef = refs(str, 'imagenes/' + arch);
  let url = await getDownloadURL(starsRef);

  document.getElementById('url').value = url;
  document.getElementById('imagen').src = url;

  alert("Imagen Descargada");

  leerInputs();

  set(ref(db, 'productos/' + codigo),{
    nombre,
    descripcion,
    precio,
    url,
    imagen,
    estatus
  }).then((res) => {
    alert("Se inserto con exito")
    console.log(url);
  }).catch((error) => {
    alert("Surgio un error" + error)
    limpiar();
  });

  mostrarProductos();
}

//Función para actualizar un producto
const actualizar = () =>{
  leerInputs();
  if(codigo == 0 || nombre === "" || descripcion === "" || precio === ""){
    alert("No puede haber campos vacios");
    limpiar();
  }else{
    update(ref(db, 'productos/' + codigo), {
      nombre: nombre,
      descripcion: descripcion,
      precio: precio
    }).then(() =>{
      alert("Se actualizo el producto");
      limpiar();
    }).catch(() =>{
      alert("Hubo un error " + error);
    });
  }

  mostrarProductos();
}

//Función para consultar un producto
const consultar = () =>{
  leerInputs();

  const dbref = ref(db);

  if(codigo <= 0){
    alert("Codigo invalido");
  }else{
    get(child(dbref, 'productos/' + codigo)).then((snapshot) =>{
      if(snapshot.exists()){
        nombre = snapshot.val().nombre;
        descripcion = snapshot.val().descripcion;
        precio = snapshot.val().precio;
        link = snapshot.val().url;
        imagen = snapshot.val().imagen;
        escribir();
        document.getElementById('imagen').src = link;
      }else{
        alert("No se encontro el registro");
        limpiar();
      }
    }).catch((error)=>{
      alert("Surgio un error " + error);
    });
  }
}

//Función para deshabilitar un producto
const desHabilitar = () =>{
  leerInputs();

  if(codigo <= 0){
    alert("Codigo invalido");
    limpiar();
  }else{
    update(ref(db, 'productos/' + codigo), {
      estatus: "1"
    }).then(() =>{
      alert("El producto se deshabilito");
      limpiar();
    }).catch(() =>{
      alert("Hubo un error " + error);
    });
  }

  mostrarProductos();
}

//Función para habilitar un producto
const habilitar = () =>{
  leerInputs();

  if(codigo <= 0){
    alert("Codigo invalido");
    limpiar();
  }else{
    update(ref(db, 'productos/' + codigo), {
      estatus: "0"
    }).then(() =>{
      alert("El producto se habilito");
      limpiar();
    }).catch(() =>{
      alert("Hubo un error " + error);
    });
  }

  mostrarProductos();
}

//Funcion para escribir en los Inputs
const escribir = () =>{
  document.getElementById('nombre').value = nombre;
  document.getElementById('descripcion').value = descripcion;
  document.getElementById('precio').value = precio;
  document.getElementById('url').value = link;
  document.getElementById('imgNombre').value = imagen;
  document.getElementById('imagen').src = url;
}

//Función para mostrar los datos
function mostrarProductos(){
  const dbRef = ref(db, 'productos');

  onValue(dbRef, (snapshot) =>{

    lis.innerHTML = `<thead><tr>
                  <th>Código</th>
                  <th>Nombre</th>
                  <th>Descripción</th>
                  <th>Precio</th>
                  <th>Nombre imagen</th>
                  <th>Imagen</th>
                  <th>Estatus</th>
            </tr><thead>` ;

    snapshot.forEach((childSnapshot) =>{
      const childKey = childSnapshot.key;
      const childData = childSnapshot.val();

      lis.lastElementChild.innerHTML += `<tr>
            <th>${childKey}</td>
            <td>${childData.nombre}</td>
            <td>${childData.descripcion}</td>
            <td>${childData.precio}</td>
            <td>${childData.imagen}</td>
            <td><img src="${childData.url}" class="imagen"></td>
            <td>${childData.estatus}</td>
      </tr>`;

      console.log(childKey + ":" + childData.nombre);
    });
  }, {
    onlyOnce: true
  });
}

//Función para cargar imagenes
async function cargarImagen(){
  const file = event.target.files[0]
  const name = event.target.files[0].name;
  document.getElementById('imgNombre').value = name;
  const storageRef = refs(str, 'imagenes/' + name);
  uploadBytes(storageRef, file).then((snapshot) => {
    //descargarImagen
    alert("Imagen cargada");
  }).catch((error) => {
    alert("Surgio un error " + error);
  });
}


//Limpiar las cajas de texto
function limpiar(){
  document.getElementById('codigo').value = 0;
  document.getElementById('nombre').value = "";
  document.getElementById('descripcion').value = "";
  document.getElementById('precio').value = "";
  document.getElementById('imgNombre').value = "";
  document.getElementById('url').value = "";
  document.getElementById('archivo').value = "";
  document.getElementById('imagen').src = "/img/default-image-5-1.jpg";
}

//Botones y acciones
archivo.addEventListener('change', cargarImagen);
btnGuardar.addEventListener('click', insestarDatos);
btnLimpiar.addEventListener('click',limpiar);
btnDesha.addEventListener('click', desHabilitar);
btnHabilitar.addEventListener('click', habilitar);
btnActualizar.addEventListener('click', actualizar);
btnConsultar.addEventListener('click', consultar);
window.onload = mostrarProductos();