// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import {getAuth, signInWithEmailAndPassword} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCbQp88i8Amsvxm6k6ZDUt8zDlCe76EJMc",
    authDomain: "webfinal-99dae.firebaseapp.com",
    projectId: "webfinal-99dae",
    storageBucket: "webfinal-99dae.appspot.com",
    messagingSenderId: "1038257620051",
    appId: "1:1038257620051:web:76abd1e067f2678827f2a6"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const auth = getAuth(app);

  //Función para la autenticacion
  async function ingreso(correo, pass) {

	try {
		await signInWithEmailAndPassword(auth, correo, pass);
        alert("Entraste");
        //Redirecciona a otra pagina
        location.href = "/Admin/administrador.html";
	} catch (error) {
        //Creacion de la ventana de error
        location.href = "/Admin/login-error.html";
	}

}

  //Función para la validacion.
  const ingresar = () =>{
    let correo = document.getElementById('correo').value;
    let pass = document.getElementById('password').value;
    

    if(!correo || !pass){
        alert("Los campos se encuentran vacios.");
        limpiar();
    }else{
        ingreso(correo, pass);
    }
  }

  //Función para limpiar las cajas de texto.
  const limpiar = () =>{
    document.getElementById('correo').value = "";
    document.getElementById('password').value = "";
  }

  document.getElementById('login').addEventListener('click',ingresar);
  document.getElementById('limpiar').addEventListener('click', limpiar);