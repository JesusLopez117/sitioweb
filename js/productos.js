// Import the functions you need from the SDKs you need
import { initializeApp } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import{ getDatabase,onValue,ref,set,child,get,update,remove}
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCbQp88i8Amsvxm6k6ZDUt8zDlCe76EJMc",
    authDomain: "webfinal-99dae.firebaseapp.com",
    projectId: "webfinal-99dae",
    storageBucket: "webfinal-99dae.appspot.com",
    messagingSenderId: "1038257620051",
    appId: "1:1038257620051:web:76abd1e067f2678827f2a6"
};

//Inicializacion del Firebase.
const app = initializeApp(firebaseConfig);
const db = getDatabase();

//Variable sobre los productos
const producto = document.getElementById('productos');

//Función para mostrar los productos
async function mostrarProductos(){
    producto.innerHTML = "";
    try{
        producto.innerHTML = "";
        const dbref = ref(db, 'productos');

        await onValue(dbref, snapshot =>{
            snapshot.forEach(childSnapshot =>{
                const childDato = childSnapshot.val();

                if(childDato.estatus === "0"){
                    producto.innerHTML += `<section>
                                    <picture><img src="${childDato.url}"></picture>
                                    <br>
                                    <h3>${childDato.nombre}</h3>
                                    <p>${childDato.descripcion}</p>
                                    <b>$${childDato.precio}</b>
                                    <button onclick="location.href='#'">Comprar</button>
                                    </section>`;
                }
            });
        });
    }catch (error) {
        console.log(error);
    }
}

window.addEventListener("DOMContentLoaded", mostrarProductos);